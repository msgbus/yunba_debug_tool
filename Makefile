.PHONY: all deps clean release

all: compile

compile: deps
	./rebar -j8 compile

deps:
	./rebar -j8 get-deps

clean:
	./rebar -j8 clean

relclean:
	rm -rf rel/yunab_debug_tool

generate: compile
	cd rel && .././rebar -j8 generate

run: generate
	./rel/yunab_debug_tool/bin/yunab_debug_tool start

console: generate
	./rel/yunab_debug_tool/bin/yunab_debug_tool console

foreground: generate
	./rel/yunab_debug_tool/bin/yunab_debug_tool foreground

erl: compile
	erl -pa ebin/ -pa deps/*/ebin/ -s yunab_debug_tool
