#! /usr/bin/env escript
%%! -smp enable debug verbose -Wall -pz ../ebin/ ../deps/gen_logger/ebin ../deps/goldrush/ebin ../deps/ibrowse/ebin ../deps/jiffy/ebin ../deps/snowflake/ebin ../deps/emqttc/ebin/ ../deps/getopt/ebin/

-include("../deps/emqttc/include/emqttc_packet.hrl").
-include("../deps/emqttc/include/emqttc_yunba_misc.hrl").

-compile(export_all).

-define(REST_BENCHMARK_DATA_DIR, "./rest_benchmark_data/").

main(Args) ->
    case length(Args) /= 4 of
        true ->
            usage();
        false ->
            pre_start(),
            [Appkey, Topic, ClientNum, TFSAddr] = Args,
            RegConfFile = ?REST_BENCHMARK_DATA_DIR ++ to_str(Appkey) ++ ".reg",
            SubscribeRecordFile = ?REST_BENCHMARK_DATA_DIR ++ to_str(Appkey) ++ "_" ++ to_str(Topic) ++ ".sub",
            {ok, RegConfFd} = file:open(RegConfFile, [read]),
            {ok, SubRecordFd} = file:open(SubscribeRecordFile, [read, write]),
            SubscribedClientNum = get_subscribed_num(SubRecordFd),
            RequestClientNum = to_int(ClientNum),
            case RequestClientNum > SubscribedClientNum of
                true ->
                    RegConf = lists:map(fun({S}) -> [ClientId, UserName, Password] = string:tokens(S, "$"),
                                       {ClientId, UserName, Password} end, read_conf(RegConfFd, [])),
                    subscribe(SubscribedClientNum +1, RequestClientNum, RegConf, Appkey, Topic, TFSAddr),
                    subsribe_record(SubscribeRecordFile, RequestClientNum);
                false ->
                    io:format("appkey ~s already subscribe topic [~s] for ~s client~n", [to_str(Appkey), to_str(Topic), to_str(RequestClientNum)])
            end
    end.

subscribe(CurrentIndex, ClientNum, RegConf, Appkey, Topic, TFSAddr) ->
    case CurrentIndex > ClientNum of
        true ->
            finished;
        false ->
            {ClientId, UserName, _Password} = lists:nth(CurrentIndex, RegConf),
            TFSURL = "http://" ++ to_str(TFSAddr) ++ "/.topicfs",
            PostContent = jiffy:encode({[
                {<<"action">>, <<"add">>},
                {<<"cid">>, to_bin(ClientId)},
                {<<"topic">>, to_bin(to_str(Appkey) ++ "/" ++ to_str(Topic))},
                {<<"score">>, <<"112">>},
                {<<"uid">>, to_bin(UserName)}
            ]}),
            {ok, _} = emqttc_utils:http_post(TFSURL, PostContent),
            timer:sleep(1000),
            io:format("client ~p subscribes topic [~s]~n", [CurrentIndex, Topic]),
            subscribe(CurrentIndex + 1, ClientNum, RegConf, Appkey, Topic, TFSAddr)
    end.

usage() ->
    io:format("Usage:~n"),
    io:format("   ./subscriber.erl <appkey> <topic> <client_num> <tfs_addr>~n").

read_conf(File, Collector) ->
    Result = file:read_line(File),
    case Result of
        eof ->
            Collector;
        {ok, Result2} ->
            Collector2 = Collector ++ [{string:strip(Result2, both, $\n)}],
            read_conf(File, Collector2)
    end.

get_subscribed_num(File) ->
    case file:read_line(File) of
        eof ->
            0;
        {ok, Result} ->
            list_to_integer(Result)
    end.

subsribe_record(FileName, SubscribedClient) ->
    file:delete(FileName),
    {ok, F} = file:open(FileName, [write]),
    io:format(F, "~s", [to_str(SubscribedClient)]).

pre_start() ->
    {ok, _} = application:ensure_all_started(ibrowse),
    {ok, _} = application:ensure_all_started(snowflake).

%%%===================================================================
%%% helper functions
%%%===================================================================
to_bin(Data) ->
    if
        is_list(Data) ->
            list_to_binary(Data);
        is_integer(Data) ->
            integer_to_binary(Data);
        is_atom(Data) ->
            atom_to_binary(Data, latin1);
        true ->
            Data
    end.

to_str(Data) ->
    if
        is_binary(Data) ->
            binary_to_list(Data);
        is_integer(Data) ->
            integer_to_list(Data);
        is_atom(Data) ->
            atom_to_list(Data);
        true ->
            Data
    end.

to_int(Data) ->
    if
        is_binary(Data) ->
            binary_to_integer(Data);
        is_list(Data) ->
            list_to_integer(Data);
        is_integer(Data) ->
            Data;
        true ->
            Data
    end.
