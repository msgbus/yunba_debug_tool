DEFAULT_ERL_R17_PATH="/opt/otp_17_5/bin"
CURRENT_ERL_PATH=`type erl | cut -d' ' -f3`

if [ "$CURRENT_ERL_PATH" != "$DEFAULT_ERL_R17_PATH/erl" ]; then
	if [ ! -d $DEFAULT_ERL_R17_PATH ]; then
		echo "Make sure your R17(or higher) Erlang version is in $DEFAULT_ERL_R17_PATH"
	else
		PATH=$DEFAULT_ERL_R17_PATH:$PATH
	fi
fi

