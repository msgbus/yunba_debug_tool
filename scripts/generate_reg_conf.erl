#! /usr/bin/env escript
%%! -smp enable debug verbose -Wall -pz ../ebin/ ../deps/gen_logger/ebin ../deps/goldrush/ebin ../deps/lager/ebin ../deps/ibrowse/ebin ../deps/jiffy/ebin ../deps/snowflake/ebin ../deps/emqttc/ebin/

-module(generate_reg_conf).

-include("../deps/emqttc/include/emqttc_packet.hrl").
-include("../deps/emqttc/include/emqttc_yunba_misc.hrl").

-compile(export_all).

-define(REST_BENCHMARK_DATA_DIR, "./rest_benchmark_data/").

generate_reg_conf(Appkey, Num) ->
    ok = application:start(ibrowse),
    ok = application:start(snowflake),

    RegRequest = #request_reg_args {
        appkey = Appkey,  %% test appkey
        platform = 2
    },

    file:make_dir(?REST_BENCHMARK_DATA_DIR),
    RegFile = ?REST_BENCHMARK_DATA_DIR ++ Appkey ++ ".reg",
    {ok, F} = file:open(RegFile, [append]),
    OldClientNum = get_lines(RegFile),
    RequestClientNum = list_to_integer(Num),
    case OldClientNum >=  RequestClientNum of
        true ->
            io:format("reg conf is already enough.~n"),
            ignore;
        false ->
            lists:map(fun(_I) -> {ok, {ClientId, UserName, Password}} = emqttc_register:register(tcp, ?YUNBA_REG_TCP_HOST, ?YUNBA_REG_TCP_PORT, RegRequest),
                R = binary_to_list(ClientId) ++ "$" ++ binary_to_list(UserName) ++ "$" ++ binary_to_list(Password),
                io:format(F, "~s~n", [R]) end, lists:seq(1, RequestClientNum - OldClientNum))
    end,
    ok.

main(Args) ->
    case length(Args) /= 2 of
        true ->
            usage();
        false ->
            [Appkey, Num] = Args,
            ok = generate_reg_conf(Appkey, Num)
    end.

get_lines(File) ->
    ClientNum = os:cmd("cat " ++ File ++ " | wc -l"),
    list_to_integer(string:strip(ClientNum, both, $\n)).

usage() ->
    io:format("Usage:~n"),
    io:format("    ./generate_reg_conf.erl <appkey> <client_num>~n").
