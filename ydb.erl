#! /usr/bin/env escript
%%! -smp enable debug verbose -Wall -pz ebin/ deps/ibrowse/ebin deps/poolboy/ebin deps/erlmongo/ebin deps/lager/ebin deps/goldrush/ebin deps/elog/ebin deps/getopt/ebin/ deps/mongodberl/ebin deps/jiffy/ebin deps/cberl/ebin

-compile(export_all).

%%%===================================================================
%%% configuration
%%%===================================================================
-define(MONGODB_REPLICA_SET, ["abj-mongo-1:27017","abj-mongorep-1:27017", "abj-mongorep-2:27017"]).
-define(MONGODB_DB_NAME, "drywall").

-define(CB_META_HOST, "abj-cbmeta-5").
-define(CB_META_USERNAME, "meta").
-define(CB_META_BUCKET, "meta").
-define(CB_META_PASSWORD, "123456").
-define(CB_META_POOLNAME, cb_meta_pool).

-define(TEST_APPKEY, "5520a2887e353f5814e10b62").
-define(NORMAL_EXIT, 0).
-define(ABNORMAL_EXIT, 1).

-define(META_KEY(APPID, MESSAGEID), <<"pub_", APPID/binary, "_", MESSAGEID/binary>>).

main([]) ->
    getopt:usage(option_spec_list(), escript:script_name());
main(Args) ->
    pre_start(),
    case getopt:parse(option_spec_list(), Args) of
        {ok, {Options, _NonOptArgs}} ->
            case proplists:get_value(mode, Options) of
                "info" ->
                    get_appkey_info(Options);
                "msg_body" ->
                    get_msg_body(Options);
                _Unknown ->
                    usage(),
                    halt(?ABNORMAL_EXIT)
            end;
        {error, {Reason, Data}} ->
            io:format("===> [ERROR] Parse command option error, reason: ~p, data: ~p~n", [Reason, Data]),
            usage(),
            halt(?ABNORMAL_EXIT)
    end,
    halt(?NORMAL_EXIT).

get_appkey_info(Options) ->
    Appkey = proplists:get_value(appkey, Options),
    case Appkey /= undefined of
        true ->
            Appid = appkey_to_appid(Appkey),
            Seckey = appkey_to_seckey(Appkey),
            io:format("~n####################~n"),
            io:format("Appkey [~p] info:~n", [Appkey]),
            io:format("Appid = ~p, Seckey = ~p~n", [Appid, Seckey]),
            io:format("####################~n"),
            halt(?NORMAL_EXIT);
        false ->
            io:format("[ERROR] => appkey is undefined~n"),
            usage(),
            halt(?ABNORMAL_EXIT)
    end.

get_msg_body(Options) ->
    MessageId = proplists:get_value(messageid, Options),
    Appkey = proplists:get_value(appkey, Options),

    case {MessageId, Appkey} of
        {undefined, _} ->
            io:format("[ERROR] => messageid is undefined~n"),
            getopt:usage(option_spec_list(), escript:script_name()),
            halt(?ABNORMAL_EXIT);
        {_, undefined} ->
            io:format("[ERROR] => appkey is undefined~n"),
            usage(),
            halt(?ABNORMAL_EXIT);
        {_, _} ->
            Appid = appkey_to_appid(Appkey),
            case Appid /= undefined of
                true ->
                    case cb_get(?CB_META_POOLNAME, ?META_KEY(Appid, Appkey)) of
                        {ok, Data} ->
                            io:format("~n####################~n"),
                            io:format("appkey [~p] message_id [~p]:~n", [Appkey, MessageId]),
                            io:format("message body is ~p", [Data]),
                            io:format("####################~n"),
                            halt(?NORMAL_EXIT);
                        {error, Reason} ->
                            io:format("[ERROR] => get message body failed ~p appkey ~p, message_id ~p~n", [Reason, Appkey, MessageId]),
                            halt(?ABNORMAL_EXIT)
                    end;
                false ->
                    io:format("[ERROR] => appid is undefined, appkey ~p~n", [Appkey]),
                    halt(?ABNORMAL_EXIT)
            end
    end.

appkey_to_appid(Appkey) ->
    case mongodberl:get_value_from_mongo(mongodbpool, incressid, to_str(Appkey)) of
        {true, Value} ->
            to_str(Value);
        Else ->
            io:format("[ERROR] => can't get appid for ~p, ~p~n", [Appkey, Else]),
            undefined
    end.

appkey_to_seckey(Appkey) ->
    case mongodberl:get_value_from_mongo(mongodbpool, seckey, Appkey) of
        {true, Value} ->
            to_str(Value);
        Else ->
            io:format("[ERROR] => can't get seckey for ~p, ~p~n", [Appkey, Else]),
            undefined
    end.

pre_start() ->
    {ok, _} = application:ensure_all_started(poolboy),
    {ok, _} = application:ensure_all_started(erlmongo),
    {ok, _} = application:ensure_all_started(mongodberl),
    {ok, _} = application:ensure_all_started(cberl),
    start_connect_mongodb(?MONGODB_REPLICA_SET, ?MONGODB_DB_NAME),
    start_connect_couchbase(?CB_META_POOLNAME, 1, ?CB_META_HOST, ?CB_META_USERNAME, ?CB_META_PASSWORD, ?CB_META_BUCKET).

usage() ->
    getopt:usage(option_spec_list(), escript:script_name()),
    mode_usage().

option_spec_list() ->
    [
        {help,          $?, "help",         undefined, "show the program options"},
        {appkey,        $a, "appkey",       string,    "appkey"},
        {messageid,     $i, "messageid",    string,    "messageid"},
        {mode,          $m, "mode",         string,    "mode"}
    ].

mode_usage() ->
    io:format("~nMode usage:~n"),
    io:format("    - info:get appid and seckey~n"),
    io:format("    - msg_body: get message body~n").

start_connect_mongodb(ReplicaSet, MongoDBName) ->
    case mongodberl:start_link({replset, {mongodbpool, {rel, ReplicaSet}, MongoDBName, 5}}) of
        {ok, _Pid} ->
            io:format("connect mongodb ~p ok~n", [ReplicaSet]);
        {error, Reason} ->
            io:format("[ERROR] connect mongodb ~p failed ~p~n", [ReplicaSet, Reason])
    end.

start_connect_couchbase(PoolName, NumCon, Host, Username, Password, BucketName) ->
    case cberl:start_link(PoolName, NumCon, Host, Username, Password, BucketName) of
        {ok, _Pid} ->
            io:format("connect meta ~p ok~n", [Host]);
        {error, Reason} ->
            io:format("[ERROR] connect meta ~p failed ~p~n", [Host, Reason])
    end.
%%%===================================================================
%%% helper functions
%%%===================================================================
to_bin(Data) ->
    if
        is_list(Data) ->
            list_to_binary(Data);
        is_integer(Data) ->
            integer_to_binary(Data);
        is_atom(Data) ->
            atom_to_binary(Data, latin1);
        true ->
            Data
    end.

to_str(Data) ->
    if
        is_binary(Data) ->
            binary_to_list(Data);
        is_integer(Data) ->
            integer_to_list(Data);
        is_atom(Data) ->
            atom_to_list(Data);
        true ->
            Data
    end.

to_int(Data) ->
    if
        is_binary(Data) ->
            binary_to_integer(Data);
        is_list(Data) ->
            list_to_integer(Data);
        is_integer(Data) ->
            Data;
        true ->
            Data
    end.

to_atom(Data) ->
    if
        is_binary(Data) ->
            binary_to_atom(Data, utf8);
        is_list(Data) ->
            list_to_atom(Data);
        is_atom(Data) ->
            Data;
        true ->
            Data
    end.

cb_get(Pool, Key) ->
    CbGetResult = try
                      cberl:get(Pool, Key)
                  catch
                      _Type:Error ->
                          {error, Error}
                  end,

    case CbGetResult of
        {_, _, DataFromCB} ->
            {ok, DataFromCB};
        {Key, {error, Reason}} ->
            {error, Reason};
        CBError ->
            {error, CBError}
    end.

get_millisec() ->
    {Mega, Sec, Micro} = os:timestamp(),
    (Mega*1000000 + Sec)*1000 + round(Micro/1000).
