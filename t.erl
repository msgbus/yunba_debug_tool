#! /usr/bin/env escript
%%! -smp enable debug verbose -Wall -pz ebin/ deps/gen_logger/ebin deps/goldrush/ebin deps/lager/ebin deps/ibrowse/ebin deps/jiffy/ebin deps/snowflake/ebin deps/emqttc/ebin/ deps/getopt/ebin/

-include("deps/emqttc/include/emqttc_packet.hrl").
-include("deps/emqttc/include/emqttc_yunba_misc.hrl").

-compile(export_all).

%% publish
-define(DEFAULT_BROKER, "localhost").
-define(DEFAULT_BROKER_PORT, 1883).
-define(DEFAULT_APPKEY, <<"5520a2887e353f5814e10b62">>).
-define(DEFAULT_SECKEY, <<"sec-SoronfkrzKCk0CJkD1hTqrINtFGVNH705CXvetQNH4L1T98G">>).
-define(DEFAULT_TOPIC, <<"yunba_debug_tool">>).
-define(DEFAULT_QOS, 1).
-define(DEFAULT_DEBUG_LEVEL, error).
-define(DEFAULT_CLIENT_NUM, 2).

%% default publisher register information
-define(DEFAULT_PUBLISHER_CLIENTID, <<"0000001528-000149000350">>).
-define(DEFAULT_PUBLISHER_USERNAME, <<"2694931056915566080">>).
-define(DEFAULT_PUBLISHER_PASSWORD, <<"d21e0c82e5153">>).

%% default subscriber register information
-define(DEFAULT_SUBSCRIBER_CLIENTID, <<"0000001528-000149000396">>).
-define(DEFAULT_SUBSCRIBER_USERNAME, <<"2695027022087458816">>).
-define(DEFAULT_SUBSCRIBER_PASSWORD, <<"f996ba54a8abe">>).

%% defalut alias
-define(DEFAULT_ALIAS, <<"yunba_debug_alias">>).

%% other configs
-define(WAITING_TIMEOUT, 10000). %% 10s
-define(NORMAL_EXIT, 0).
-define(ABNORMAL_EXIT, 1).

main([]) ->
    getopt:usage(option_spec_list(), escript:script_name());
main(Args) ->
    OptSpecList = option_spec_list(),
    pre_start(),
    case getopt:parse(option_spec_list(), Args) of
        {ok, {Options, _NonOptArgs}} ->
            case proplists:get_value(mode, Options) of
                "simple" ->
                    do_simple_publish(Options);
                "benchmark" ->
                    do_benchmark_publish(Options);
                "keep_alive" ->
                    do_keep_alive(Options);
                "alias" ->
                    do_publish_to_alias(Options);
                _Unknown ->
                    getopt:usage(OptSpecList, escript:script_name()),
                    halt(?ABNORMAL_EXIT)
            end;
        {error, {Reason, Data}} ->
            io:format("===> [ERROR] Parse command option error, reason: ~p, data: ~p~n", [Reason, Data]),
            getopt:usage(OptSpecList, escript:script_name()),
            halt(?ABNORMAL_EXIT)
    end,
    halt(?NORMAL_EXIT).

do_simple_publish(Options) ->
    Broker = proplists:get_value(broker, Options, ?DEFAULT_BROKER),
    Appkey = to_bin(proplists:get_value(appkey, Options, ?DEFAULT_APPKEY)),
    Topic = to_bin(proplists:get_value(topic, Options, ?DEFAULT_TOPIC)),
    QoS = to_int(proplists:get_value(qos, Options, ?DEFAULT_QOS)),
    DebugLevel = to_atom(proplists:get_value(debug_level, Options, ?DEFAULT_DEBUG_LEVEL)),
    Parent = self(),
    ClientNum = 1,
    %% create publisher process
    Publisher = spawn(fun() -> publisher_process(Parent, [Broker, Appkey, Topic, QoS, DebugLevel, ClientNum]) end),

    %% create subscriber process
    Subsciber = spawn(fun() -> subscriber_process(Parent, Publisher, ClientNum, [Broker, Appkey, Topic, QoS, DebugLevel]) end),

    monitor(process, Publisher),
    monitor(process, Subsciber),

    wait([normal, normal], []).

do_benchmark_publish(Options) ->
    Broker = proplists:get_value(broker, Options, ?DEFAULT_BROKER),
    Appkey = to_bin(proplists:get_value(appkey, Options, ?DEFAULT_APPKEY)),
    Topic = to_bin(proplists:get_value(topic, Options, ?DEFAULT_TOPIC)),
    QoS = to_int(proplists:get_value(qos, Options, ?DEFAULT_QOS)),
    DebugLevel = to_atom(proplists:get_value(debug_level, Options, ?DEFAULT_DEBUG_LEVEL)),
    ClientNum = to_int(proplists:get_value(client_num, Options, ?DEFAULT_CLIENT_NUM)),
    Parent = self(),

    Publisher = spawn(fun() -> publisher_process(Parent, [Broker, Appkey, Topic, QoS, DebugLevel, ClientNum]) end),
    Subscribers = [spawn(fun() -> subscriber_process(Parent, Publisher, I, [Broker, Appkey, Topic, QoS, DebugLevel]) end) || I <- lists:seq(1, ClientNum)],

    monitor(process, Publisher),
    [monitor(process, Subscriber) || Subscriber <- Subscribers],

    wait(lists:duplicate(ClientNum + 1, normal), []).

do_keep_alive(Options) ->
    Broker = proplists:get_value(broker, Options, ?DEFAULT_BROKER),
    Appkey = to_bin(proplists:get_value(appkey, Options, ?DEFAULT_APPKEY)),
    Topic = to_bin(proplists:get_value(topic, Options, ?DEFAULT_TOPIC)),
    QoS = to_int(proplists:get_value(qos, Options, ?DEFAULT_QOS)),
    DebugLevel = to_atom(proplists:get_value(debug_level, Options, ?DEFAULT_DEBUG_LEVEL)),

    {ClientId, UserName, Password} = case emqttc_register:register(?YUNBA_REG_URL, Appkey, ?TEST_PLATFORM) of
                                         {ok, {ClientId1, UserName1, Password1}} ->
                                             {ClientId1, UserName1, Password1};
                                         {error, register_failed} ->
                                             io:format("===> [ERROR] register error, use default register information~n"),
                                             {?DEFAULT_PUBLISHER_CLIENTID, ?DEFAULT_PUBLISHER_USERNAME, ?DEFAULT_PUBLISHER_PASSWORD}
                                     end,

    io:format("===> [OK][publisher] Broker = ~p, Appkey = ~p, Topic = ~p, QoS = ~p,
                    ClientId = ~p, UserName = ~p, Password = ~p, DebugLevel = ~p~n",
        [Broker, Appkey, Topic, QoS, ClientId, UserName, Password, DebugLevel]),

    {ok, Client} = emqttc:start_link([{host, Broker}, {port, ?DEFAULT_BROKER_PORT}, {client_id, ClientId},
                                      {proto_ver, ?MQTT_PROTO_V31_YUNBA}, {username, UserName},
                                      {password, Password}, {logger, DebugLevel}]),

    emqttc:subscribe(Client, Topic, QoS),
    receive
        {suback, MessageId} ->
            io:format("===> [OK][subscriber] Received suback of ~p~n", [MessageId]),
            {ok, subscribe_finished}
    after
        ?WAITING_TIMEOUT ->
            io:format("===> [ERROR][subscriber] Waiting for suback timeout~n"),
            {error, receive_suback_timeout}
    end,

    receive
        {publish, Topic, Payload} ->
            io:format("===> [OK][subscriber] Message received, Topic: ~p, Payload: ~p~n", [Topic, Payload]),
            {ok, subscriber_message_received}
    end,

    ok.

do_publish_to_alias(Options) ->
    Broker = proplists:get_value(broker, Options, ?DEFAULT_BROKER),
    Alias = proplists:get_value(alias, Options, ?DEFAULT_ALIAS),
    Appkey = to_bin(proplists:get_value(appkey, Options, ?DEFAULT_APPKEY)),
    QoS = to_int(proplists:get_value(qos, Options, ?DEFAULT_QOS)),
    DebugLevel = to_atom(proplists:get_value(debug_level, Options, ?DEFAULT_DEBUG_LEVEL)),

    {ClientId, UserName, Password} = case emqttc_register:register(?YUNBA_REG_URL, Appkey, ?TEST_PLATFORM) of
                                         {ok, {ClientId1, UserName1, Password1}} ->
                                             {ClientId1, UserName1, Password1};
                                         {error, register_failed} ->
                                             io:format("===> [ERROR] register error, use default register information~n"),
                                             {?DEFAULT_PUBLISHER_CLIENTID, ?DEFAULT_PUBLISHER_USERNAME, ?DEFAULT_PUBLISHER_PASSWORD}
                                     end,

    io:format("===> [OK][publisher] Broker = ~p, Appkey = ~p, QoS = ~p,
                    ClientId = ~p, UserName = ~p, Password = ~p, DebugLevel = ~p~n",
        [Broker, Appkey, QoS, ClientId, UserName, Password, DebugLevel]),

    {ok, Client} = emqttc:start_link([{host, Broker}, {port, ?DEFAULT_BROKER_PORT}, {client_id, ClientId},
                                      {proto_ver, ?MQTT_PROTO_V31_YUNBA}, {username, UserName},
                                      {password, Password}, {logger, DebugLevel}]),

    %% sets client as alias
    emqttc_alias:set_alias(Client, Alias),
    receive
        {puback, MessageId} ->
            io:format("===> [OK][publisher] Received puback of ~p~n", [MessageId])
    after
        ?WAITING_TIMEOUT ->
            io:format("===> [ERROR][publisher] Waiting for puback timeout~n"),
            exit(receive_puback_timeout)
    end,

    %% gets client's alias
    io:format("Client's alias is ~p~n", [emqttc_alias:get_alias(Client)]),

    %% publish to client use alias
    publish_to_alias(Client, Alias, ?TEST_PAYLOAD),

    receive
        {publish, Topic, Payload} ->
            io:format("===> [OK]Message received, Topic: ~p, Payload: ~p~n", [Topic, Payload]),
            {ok, subscriber_message_received}
    after
        ?WAITING_TIMEOUT ->
            io:format("===> [ERROR]Receive publish message timeout!~n"),
            {error, subscriber_message_received_timeout}
    end,

    emqttc:disconnect(Client),
    ok.

wait(ExpectedResult, ResultCollector) ->
    receive
        {'DOWN', _Ref, process, Pid, Reason} ->
            case Reason of
                normal ->
                    ResultCollector2 = ResultCollector ++ [normal],
                    case ResultCollector2 /= ExpectedResult of
                        true ->
                            wait(ExpectedResult, ResultCollector2);
                        false ->
                            halt(?NORMAL_EXIT)
                    end;
                Else ->
                    io:format("===> [ERROR] Process ~p is crashed because of ~p~n", [Pid, Else]),
                    halt(?ABNORMAL_EXIT)
            end;
        Else ->
            io:format("===> [ERROR] Parent receives unknown signal ~p~n", [Else]),
            halt(?ABNORMAL_EXIT)
    after ?WAITING_TIMEOUT ->
        io:format("===> [ERROR] Parent wait children stop finish job timeout~n"),
        halt(?ABNORMAL_EXIT)
    end,
    halt(?NORMAL_EXIT).

publisher_process(_Parent, Args) ->
    [Broker, Appkey, Topic, QoS, DebugLevel, ClientNum] = Args,
    {ClientId, UserName, Password} = case emqttc_register:register(?YUNBA_REG_URL, Appkey, ?TEST_PLATFORM) of
                                         {ok, {ClientId1, UserName1, Password1}} ->
                                             {ClientId1, UserName1, Password1};
                                         {error, register_failed} ->
                                             io:format("===> [ERROR] register error, use default register information~n"),
                                             {?DEFAULT_PUBLISHER_CLIENTID, ?DEFAULT_PUBLISHER_USERNAME, ?DEFAULT_PUBLISHER_PASSWORD}
                                     end,

    io:format("===> [OK][publisher] Broker = ~p, Appkey = ~p, Topic = ~p, QoS = ~p,
                    ClientId = ~p, UserName = ~p, Password = ~p, DebugLevel = ~p~n",
        [Broker, Appkey, Topic, QoS, ClientId, UserName, Password, DebugLevel]),

    {ok, Client} = emqttc:start_link([{host, Broker}, {port, ?DEFAULT_BROKER_PORT}, {client_id, ClientId},
                                      {proto_ver, ?MQTT_PROTO_V31_YUNBA}, {username, UserName},
                                      {password, Password}, {logger, DebugLevel}]),
    case wait2([], ClientNum, subscribe_finished) of
        {finished, Subscribers1} ->
            Subcribers2 = [ SubscriberIndex || {SubscriberIndex, _Timestamp} <- Subscribers1],
            io:format("===> [OK][publisher] The subscriber process ~p subscribe topic ~p successfully~n", [Subcribers2, Topic]),
            publish(Client, Topic, ?TEST_PAYLOAD, QoS),
            PublishTimestamp = get_millisec(),
            case wait2([], ClientNum, subscriber_message_received) of
                {finished, Subscribers3} ->
                    ReceivedTimestamps = [ Timestamp || {_SubscriberIndex, Timestamp} <- Subscribers3],
                    MaxReceivedTime = lists:max(ReceivedTimestamps) - PublishTimestamp,
                    MinReceivedTime = lists:min(ReceivedTimestamps) - PublishTimestamp,
                    io:format("===> [OK][publisher] The subscribers have received message, MaxReceivedTime = ~pms, MinReceivedTime = ~pms~n",
                              [MaxReceivedTime, MinReceivedTime]);
                timeout ->
                    io:format("===> [ERROR][publisher] The subscriber process receives message timeout~n"),
                    exit(subscriber_message_received_timeout)
            end;
        timeout ->
            io:format("===> [ERROR][publisher] Wait the subscriber process subscribes topic timeout"),
            exit(waiting_subscribe_timeout)
    end.

wait2(Collector, ClientNum, Signal) ->
    receive
        {Signal, {SubscriberIndex, Timestamp}} ->
            io:format("receive ~p from subscriber-~p~n", [Signal, SubscriberIndex]),
            Collector2 = Collector ++ [{SubscriberIndex, Timestamp}],
            case length(Collector2) of
                ClientNum ->
                    {finished, Collector2};
                _Else ->
                    wait2(Collector2, ClientNum, Signal)
            end
    after ?WAITING_TIMEOUT ->
        io:format("===> [ERROR][publisher] Wait the subscriber process subscribes topic timeout"),
        timeout
    end.

subscriber_process(_Parent, Publisher, SubscriberIndex, Args) ->
    [Broker, Appkey, Topic, QoS, DebugLevel] = Args,
    {ClientId, UserName, Password} = case emqttc_register:register(?YUNBA_REG_URL, Appkey, ?TEST_PLATFORM) of
                                         {ok, {ClientId1, UserName1, Password1}} ->
                                             {ClientId1, UserName1, Password1};
                                         {error, register_failed} ->
                                             io:format("===> [ERROR] register error, use default register information~n"),
                                             {?DEFAULT_SUBSCRIBER_CLIENTID, ?DEFAULT_SUBSCRIBER_USERNAME, ?DEFAULT_SUBSCRIBER_PASSWORD}
                                     end,

    io:format("===> [OK][subscriber-~p] Broker = ~p, Appkey = ~p, Topic = ~p, QoS = ~p,
                    ClientId = ~p, UserName = ~p, Password = ~p, DebugLevel = ~p~n",
              [SubscriberIndex, Broker, Appkey, Topic, QoS, ClientId, UserName, Password, DebugLevel]),

    {ok, Client} = emqttc:start_link([{host, Broker}, {port, ?DEFAULT_BROKER_PORT}, {client_id, ClientId},
                                      {proto_ver, ?MQTT_PROTO_V31_YUNBA}, {username, UserName},
                                      {password, Password}, {logger, error}]),

    case subscribe(SubscriberIndex, Client, Topic, QoS) of
        {ok, subscribe_finished} ->
            Publisher !{subscribe_finished, {SubscriberIndex, get_millisec()}};
        {error, Reason1} ->
            exit(Reason1)
    end,

    case receive_publish_message(SubscriberIndex) of
        {ok, subscriber_message_received} ->
            Publisher ! {subscriber_message_received, {SubscriberIndex, get_millisec()}};
        {error, Reason2} ->
            exit(Reason2)
    end,

    case unsubscribe(SubscriberIndex, Client, Topic) of
        {ok, unsub_finished} ->
            ignore;
        {error, Reason3} ->
            exit(Reason3)
    end,

    disconnect(Client).

publish(Client, Topic, Payload, QoS) ->
    io:format("===> [OK][publisher] publish to topic:~p payload:~p qos:~p~n", [Topic, Payload, QoS]),
    case QoS of
        ?QOS_0 ->
            publish_qos0(Client, Topic, Payload);
        ?QOS_1 ->
            publish_qos1(Client, Topic, Payload);
        ?QOS_2 ->
            publish_qos2(Client, Topic, Payload)
    end.

publish_qos0(Client, Topic, Payload) ->
    emqttc:publish(Client, Topic, Payload, qos0).

publish_qos1(Client, Topic, Payload) ->
    emqttc:publish(Client, Topic, Payload, qos1),
    receive
        {puback, MessageId} ->
            io:format("===> [OK][publisher] Received puback of ~p~n", [MessageId])
    after
        ?WAITING_TIMEOUT ->
            io:format("===> [ERROR][publisher] Waiting for puback timeout~n"),
            exit(receive_puback_timeout)
    end.

publish_qos2(Client, Topic, Payload) ->
    emqttc:publish(Client, Topic, Payload, qos2),
    receive
        {pubcomp, MessageId} ->
            io:format("===> [OK][publisher] Received pubcomp of ~p~n", [MessageId])
    after
        ?WAITING_TIMEOUT ->
            io:format("===> [ERROR][publisher] Waiting for pubcomp timeout~n"),
            exit(receive_pubcomp_timeout)
    end.

publish_to_alias(Client, Alias, Payload) ->
    emqttc_alias:publish_to_alias(Client, Alias, Payload),
    receive
        {puback, MessageId} ->
            io:format("===> [OK][publisher] Received puback of ~p~n", [MessageId])
    after
        ?WAITING_TIMEOUT ->
            io:format("===> [ERROR][publisher] Waiting for puback timeout~n"),
            exit(receive_puback_timeout)
    end.

receive_publish_message(SubscriberIndex) ->
    receive
        {publish, Topic, Payload} ->
            io:format("===> [OK][subscriber-~p] Message received, Topic: ~p, Payload: ~p~n", [SubscriberIndex, Topic, Payload]),
            {ok, subscriber_message_received}
    after
        ?WAITING_TIMEOUT ->
            io:format("===> [ERROR][subscriber-~p] Receive publish message timeout!~n", [SubscriberIndex]),
            {error, subscriber_message_received_timeout}
    end.

subscribe(SubscriberIndex, Client, Topic, QoS) ->
    emqttc:subscribe(Client, Topic, QoS),
    receive
        {suback, MessageId} ->
            io:format("===> [OK][subscriber-~p] Received suback of ~p~n", [SubscriberIndex, MessageId]),
            {ok, subscribe_finished}
    after
        ?WAITING_TIMEOUT ->
            io:format("===> [ERROR][subscriber-~p] Waiting for suback timeout~n", [SubscriberIndex]),
            {error, receive_suback_timeout}
    end.

unsubscribe(SubscriberIndex, Client, Topic) ->
    emqttc:unsubscribe(Client, Topic),
    receive
        {unsuback, MessageId} ->
            io:format("===> [OK][subscriber-~p] Received unsuback of ~p~n", [SubscriberIndex, MessageId]),
            {ok, unsub_finished}
    after
        ?WAITING_TIMEOUT ->
            io:format("===> [ERROR][subscriber-~p] Waiting for unsuback timeout~n", [SubscriberIndex]),
            {error, receive_unsuback_timeout}
    end.

disconnect(Client) ->
    emqttc:disconnect(Client).

option_spec_list() ->
    [
        {help,          $?, "help",         undefined, "show the program options"},
        {broker,        $h, "broker",       string,    "broker domain name"},
        {appkey,        $a, "appkey",       string,    "publish appkey"},
        {seckey,        $s, "seckey",       string,    "seckey with the appkey"},
        {topic,         $t, "topic",        string,    "publish topic"},
        {qos,           $q, "qos",          string,    "qos"},
        {alias,         $l, "alias",        string,    "the client alias"},
        {debug_level,   $d, "debug",        string,    "debug level, can be info / error / debug, default is error "},
        {client_num,    $c, "client",       string,    "the number of subscriber client when running benchmark"},
        {mode,          $m, "mode",         string,    "test mode, can use simple / benchmark / keep_alive / alias"}
    ].

pre_start() ->
    ok = application:start(ibrowse),  %% start ibrowse to support http get/post
    ok = application:start(snowflake).

%%%===================================================================
%%% helper functions
%%%===================================================================
to_bin(Data) ->
    if
        is_list(Data) ->
            list_to_binary(Data);
        is_integer(Data) ->
            integer_to_binary(Data);
        is_atom(Data) ->
            atom_to_binary(Data, latin1);
        true ->
            Data
    end.

to_str(Data) ->
    if
        is_binary(Data) ->
            binary_to_list(Data);
        is_integer(Data) ->
            integer_to_list(Data);
        is_atom(Data) ->
            atom_to_list(Data);
        true ->
            Data
    end.

to_int(Data) ->
    if
        is_binary(Data) ->
            binary_to_integer(Data);
        is_list(Data) ->
            list_to_integer(Data);
        is_integer(Data) ->
            Data;
        true ->
            Data
    end.

to_atom(Data) ->
    if
        is_binary(Data) ->
            binary_to_atom(Data, utf8);
        is_list(Data) ->
            list_to_atom(Data);
        is_atom(Data) ->
            Data;
        true ->
            Data
    end.

get_millisec() ->
    {Mega, Sec, Micro} = os:timestamp(),
    (Mega*1000000 + Sec)*1000 + round(Micro/1000).