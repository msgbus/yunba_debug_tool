#! /usr/bin/env escript
%%! -smp enable debug verbose -Wall -pz ebin/ deps/gen_logger/ebin deps/goldrush/ebin deps/lager/ebin deps/ibrowse/ebin deps/jiffy/ebin deps/snowflake/ebin deps/emqttc/ebin/ deps/getopt/ebin/ deps/lager/ebin deps/goldrush/ebin

-include("deps/emqttc/include/emqttc_packet.hrl").
-include("deps/emqttc/include/emqttc_yunba_misc.hrl").

-compile(export_all).

%% Timeout configs
-define(DEFAULT_BENCKMARK_TIMEOUT,    10000).      %% 10s
-define(WAITING_CONNECT_TIMEOUT,      3000).       %% 3s
-define(WAITING_MSG_RECEIVED_TIMEOUT, 3000).       %% 3s

%% Exit code
-define(NORMAL_EXIT,   0).
-define(ABNORMAL_EXIT, 1).

-define(REST_BENCHMARK_TOPIC, "yunba_rest_benchmark").
-define(GENERATE_REG_CONF, "generate_reg_conf.erl").
-define(SUBSCRIBE_TOPIC, "subscribe_topic.erl").
-define(REST_BENCHMARK_DATA_DIR, "./scripts/rest_benchmark_data/").

-record(benchmark_args, {
    appkey,
    seckey,
    topic,
    rest_host,
    rest_port,
    client_num,
    debug_level,
    benchmark_timeout
}).

-record(reg_info, {
    clientid,
    username,
    password
}).

-define(OPT_SPEC_LIST,
    [
        {help,              $?, "help",              undefined, "show the program options"},
        {appkey,            $a, "appkey",            string,    "publish appkey"},
        {seckey,            $s, "seckey",            string,    "seckey"},
        {debug_level,       $d, "debug",             string,    "debug level, can be info / error / debug, default is error "},
        {client_num,        $c, "client",            string,    "the number of subscriber client when running benchmark"},
        {rest_host,         $h, "RESTful host",      string,    "RESTful host"},
        {rest_port,         $p, "RESTful port",      string,    "RESTful port"},
        {benchmark_timeout, $b, "benchmark timeout", string,    "the entire benchmark process timeout"}
    ]).

main([]) ->
    getopt:usage(?OPT_SPEC_LIST, escript:script_name());
main(Args) ->
    case length(Args) >= 10 of
        true ->
            pre_start(),
            case getopt:parse(?OPT_SPEC_LIST, Args) of
                {ok, {Options, _NonOptArgs}} ->
                    case parse_args(Options) of
                        {ok, BenchmarkArgs} ->
                            do_benchmark_publish(BenchmarkArgs),
                            halt(?NORMAL_EXIT);
                        {error, _Reason} ->
                            io:format("parse command line args failed !~n"),
                            getopt:usage(?OPT_SPEC_LIST, escript:script_name()),
                            halt(?ABNORMAL_EXIT)
                    end;
                {error, {_Reason, _Data}} ->
                    io:format("parse command line args failed !~n"),
                    getopt:usage(?OPT_SPEC_LIST, escript:script_name()),
                    halt(?ABNORMAL_EXIT)
            end,
            halt(?NORMAL_EXIT);
        false ->
            getopt:usage(?OPT_SPEC_LIST, escript:script_name()),
            halt(?ABNORMAL_EXIT)
    end.

parse_args(Options) ->
    Appkey = proplists:get_value(appkey, Options),
    Seckey = proplists:get_value(seckey, Options),
    Topic = ?REST_BENCHMARK_TOPIC,
    RestHost = proplists:get_value(rest_host, Options),
    RestPort = proplists:get_value(rest_port, Options),
    ClientNum = proplists:get_value(client_num, Options),
    DebugLevel = proplists:get_value(debug_level, Options, error),
    BenchmarkTimeout = proplists:get_value(benchmark_timeout, Options, ?DEFAULT_BENCKMARK_TIMEOUT),

    case {Appkey, Seckey, Topic, RestHost, RestPort, ClientNum} of
        {undefined, _, _, _, _, _} ->
            {error, undefined_appkey};
        {_, undefined, _, _, _, _} ->
            {error, undefined_seckey};
        {_, _, undefined, _, _, _} ->
            {error, undefined_topic};
        {_, _, _, undefined, _, _} ->
            {error, undefined_rest_host};
        {_, _, _, _, undefined, _} ->
            {error, undefined_rest_port};
        {_, _, _, _, _, undefined} ->
            {error, undefined_client_num};
        {_, _, _, _, _, _} ->
            {ok, #benchmark_args {
                appkey = to_bin(Appkey),
                seckey = to_bin(Seckey),
                topic = to_bin(Topic),
                rest_host = to_str(RestHost),
                rest_port = to_int(RestPort),
                client_num = to_int(ClientNum),
                debug_level = to_atom(DebugLevel),
                benchmark_timeout = to_int(BenchmarkTimeout)
            }}
    end.

do_benchmark_publish(BenchmarkArgs) ->
    #benchmark_args{
        appkey = Appkey,
        seckey = _Seckey,
        topic = Topic,
        rest_host = RestHost,
        rest_port = RestPort,
        client_num = ClientNum,
        debug_level = _DebugLevel,
        benchmark_timeout = BenchmarkTimeout
    } = BenchmarkArgs,

    prepare_rest_benckmark(Appkey, Topic, ClientNum),

    Parent = self(),
    RegConfFile = ?REST_BENCHMARK_DATA_DIR ++ to_str(Appkey) ++ ".reg",
    {ok, F} = file:open(RegConfFile, [read]),
    RegConf = lists:map(fun({S}) -> [ClientId, UserName, Password] = string:tokens(S, "$"), {ClientId, UserName, Password} end, read_conf(F, [])),

    Publisher = spawn(fun() -> publisher_process(Parent, BenchmarkArgs) end),
    Subscribers = spawn_client(Parent, Publisher, BenchmarkArgs, RegConf, BenchmarkArgs#benchmark_args.client_num, []),

    RestURL = "http://" ++ to_str(RestHost) ++ ":" ++ to_str(RestPort) ++ "/",
    io:format("Running ~s client benchmark test @ ~s~n", [to_str(ClientNum), to_str(RestURL)]),
    io:format("   topic:~s, appkey:~s~n", [to_str(Topic), to_str(Appkey)]),

    monitor(process, Publisher),
    [monitor(process, Subscriber) || Subscriber <- Subscribers],

    wait(ClientNum + 1, [], BenchmarkTimeout).

spawn_client(Parent, Publisher, BenchmarkArgs, RegConf, ClientNum, Pids) ->
    case ClientNum of
        0 ->
            Pids;
        _ ->
            {ClientId, UserName, Password} = lists:nth(ClientNum, RegConf),
            RegInfo = #reg_info {
                clientid = ClientId, username = UserName, password = Password
            },
            Pid = spawn(fun() -> subscriber_process(Parent, Publisher, ClientNum, RegInfo, BenchmarkArgs) end),
            Pids2 = Pids ++ [Pid],
            spawn_client(Parent, Publisher, BenchmarkArgs, RegConf, ClientNum - 1, Pids2)
    end.

wait(ExpectedResult, ResultCollector, Timeout) ->
    receive
        {'DOWN', _Ref, process, _Pid, Reason} ->
            ResultCollector2 = case Reason of
                                   normal ->
                                       ResultCollector ++ [normal];
                                   _Crashed ->
                                       ResultCollector ++ [crashed]
                               end,
            case length(ResultCollector2) of
                ExpectedResult ->
                    halt(?ABNORMAL_EXIT);
                _ ->
                    wait(ExpectedResult, ResultCollector2, Timeout)
            end
    after Timeout ->
        io:format("ResultCollector2 = ~p~n", [ResultCollector]),
        halt(?ABNORMAL_EXIT)
    end.

publisher_process(_Parent, BenchmarkArgs) ->
    #benchmark_args {
        appkey = _Appkey,
        seckey = _Seckey,
        topic = _Topic,
        rest_host = _RestHost,
        rest_port = _RestPort,
        client_num = ClientNum,
        debug_level = _DebugLevel
    } = BenchmarkArgs,

    WaitingConnectTimeout = ?WAITING_CONNECT_TIMEOUT * ClientNum,

    case wait2([], ClientNum, connect_ok, WaitingConnectTimeout) of
        {finished, _Subscribers1} ->
            PublishTimestamp = get_millisec(),
            case rest_publish(BenchmarkArgs) of
                ok ->
                    case wait2([], ClientNum, message_received, ?WAITING_MSG_RECEIVED_TIMEOUT) of
                        {_, Subscribers3} ->
                            ReceivedTimestamps = [ Timestamp || {_SubscriberIndex, Timestamp} <- Subscribers3],
                            MaxReceivedTime = lists:max(ReceivedTimestamps) - PublishTimestamp,
                            MinReceivedTime = lists:min(ReceivedTimestamps) - PublishTimestamp,
                            ReceivedNum = length(ReceivedTimestamps),
                            AverageReceivedTime = round((lists:sum(ReceivedTimestamps) - ReceivedNum * PublishTimestamp)/ ReceivedNum),
                            output_result(AverageReceivedTime, MaxReceivedTime, MinReceivedTime, ReceivedNum, BenchmarkArgs)
                    end;
                {error, Response} ->
                    io:format("RESTful publish failed, response ~p~n", [Response]),
                    halt(?ABNORMAL_EXIT)
            end;
        {timeout, _} ->
            io:format("running ~p client failed, connect front timeout !~n", [ClientNum]),
            halt(?ABNORMAL_EXIT)
    end.

wait2(Collector, ClientNum, Signal, Timeout) ->
    receive
        {Signal, {SubscriberIndex, Timestamp}} ->
            Collector2 = Collector ++ [{SubscriberIndex, Timestamp}],
            case length(Collector2) of
                ClientNum ->
                    {finished, Collector2};
                _Else ->
                    wait2(Collector2, ClientNum, Signal, Timeout)
            end
    after Timeout ->
        {timeout, Collector}
    end.

rest_publish(BenchmarkArgs) ->
    #benchmark_args {
        appkey = Appkey,
        seckey = Seckey,
        topic = Topic,
        rest_host = RestHost,
        rest_port = RestPort,
        client_num = _ClientNum,
        debug_level = _DebugLevel
    } = BenchmarkArgs,
    RestURL = "http://" ++ to_str(RestHost) ++ ":" ++ to_str(RestPort),
    PostContent = jiffy:encode({[
        {<<"method">>, <<"publish">>},
        {<<"appkey">>, to_bin(Appkey)},
        {<<"seckey">>, to_bin(Seckey)},
        {<<"topic">>, to_bin(Topic)},
        {<<"msg">>, <<"yunba_rest_benchmark">>},
        {<<"opts">>, {[{<<"time_to_live">>, 0}]}}
        ]}),
    case emqttc_utils:http_post(RestURL, PostContent) of
        {ok, Body} ->
            BodyBin = emqttc_utils:to_bin(Body),
            {Response} = jiffy:decode(BodyBin),
            Status = proplists:get_value(<<"status">>, Response),
            case Status of
                0 ->
                    ok;
                _ ->
                    {error, Response}
            end;
        {error, Reason} ->
            {error, Reason}
    end.

subscriber_process(_Parent, Publisher, I, RegInfo, BenchmarkArgs)  ->
    #benchmark_args {
        appkey = Appkey,
        seckey = _Seckey,
        topic = Topic,
        rest_host = _RestHost,
        rest_port = _RestPort,
        client_num = _ClientNum,
        debug_level = DebugLevel
    } = BenchmarkArgs,

    TicketRequest = #request_ticket_args {
        appkey = Appkey,
        type = ip
    },

    #reg_info {
        clientid = ClientId,
        username = UserName,
        password = Password
    } = RegInfo,

    {ok, {Host, Port}} = emqttc_broker:get_broker(tcp, ?YUNBA_TICKET_TCP_HOST, ?YUNBA_TICKET_TCP_PORT, TicketRequest),

    {ok, Client} = emqttc:start_link([{host, Host}, {port, Port}, {client_id, to_bin(ClientId)},
                                      {proto_ver, ?MQTT_PROTO_V31_YUNBA}, {username, to_bin(UserName)},
                                      {password, to_bin(Password)}, {logger, DebugLevel}]),

    receive
        {mqttc, _Pid, connected} ->
            Publisher !{connect_ok, {I, get_millisec()}}
    end,

    receive
        {publish, Topic, _Payload} ->
            Publisher ! {message_received, {I, get_millisec()}}
    end,

    disconnect(Client).

disconnect(Client) ->
    emqttc:disconnect(Client).

pre_start() ->
    {ok, _} = application:ensure_all_started(ibrowse),
    {ok, _} = application:ensure_all_started(snowflake).

output_result(AverageReceivedTime, MaxReceivedTime, MinReceivedTime, ReceivedNum, BenchmarkArgs) ->
    #benchmark_args {
        appkey = _Appkey,
        seckey = _Seckey,
        topic = _Topic,
        rest_host = _RestHost,
        rest_port = _RestPort,
        client_num = ClientNum,
        debug_level = _DebugLevel
    } = BenchmarkArgs,
    io:format("   avg: ~p ms, max: ~p ms, min: ~p ms, timeout: ~p~n", [AverageReceivedTime, MaxReceivedTime, MinReceivedTime, ClientNum - ReceivedNum]).

read_conf(File, Collector) ->
    Result = file:read_line(File),
    case Result of
        eof ->
            Collector;
        {ok, Result2} ->
            Collector2 = Collector ++ [{string:strip(Result2, both, $\n)}],
            read_conf(File, Collector2)
    end.

prepare_rest_benckmark(Appkey, Topic, ClientNum) ->
    io:format("Preparing REST benckmark ...~n"),
    GenerateRegCommands = [
            "cd scripts;./" ++ ?GENERATE_REG_CONF ++ " " ++ to_str(Appkey) ++ " " ++ to_str(ClientNum)
    ],
    SubscribeTopicCommands = [
            "cd scripts;./" ++ ?SUBSCRIBE_TOPIC ++  " " ++ to_str(Appkey) ++  " " ++ to_str(Topic) ++  " " ++ to_str(ClientNum)
    ],
    os:cmd(GenerateRegCommands),
    os:cmd(SubscribeTopicCommands).

%%%===================================================================
%%% helper functions
%%%===================================================================
to_bin(Data) ->
    if
        is_list(Data) ->
            list_to_binary(Data);
        is_integer(Data) ->
            integer_to_binary(Data);
        is_atom(Data) ->
            atom_to_binary(Data, latin1);
        true ->
            Data
    end.

to_str(Data) ->
    if
        is_binary(Data) ->
            binary_to_list(Data);
        is_integer(Data) ->
            integer_to_list(Data);
        is_atom(Data) ->
            atom_to_list(Data);
        true ->
            Data
    end.

to_int(Data) ->
    if
        is_binary(Data) ->
            binary_to_integer(Data);
        is_list(Data) ->
            list_to_integer(Data);
        is_integer(Data) ->
            Data;
        true ->
            Data
    end.

to_atom(Data) ->
    if
        is_binary(Data) ->
            binary_to_atom(Data, utf8);
        is_list(Data) ->
            list_to_atom(Data);
        is_atom(Data) ->
            Data;
        true ->
            Data
    end.

get_millisec() ->
    {Mega, Sec, Micro} = os:timestamp(),
    (Mega*1000000 + Sec)*1000 + round(Micro/1000).
