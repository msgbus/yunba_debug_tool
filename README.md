## Overview

`yunba_debug_tool` 项目主要提供针对 Yunba 后端系统的 debug 工具，其中包括 benchmark 工具，数据库查询工具等。

## How to compile

在很多 debug 场景下，需要使用到 MQTT 客户端，由于本项目用到了 [yunba-erlang-sdk](https://github.com/yunba/yunba-erlang-sdk.git)，所以需要使用 R17 或者更高版本的 Erlang。

如果运行环境中安装了 R16B02（目前服务器版本）或者更高版本的 Erlang 运行环境，则需要在执行 make 之前，执行：

    $ source scripts/get_erl_r17.sh 
	
这里会假设你当前更高版本的 Erlang 是安装在 `/opt/otp_17_5/` 下（如果不是，可以修改 `get_erl_r17.sh` 来改变这个目录）。

一般使用只需要：

    $ make
	
即可。


## How to use rest_benchmark

执行 

    $ ./rest_benchmark.erl --help

有非常详细的说明文档。

典型地：

    $ ./rest_benchmark.erl -h rest.yunba.io -p 8080 -c 20 -a  5520a2887e353f5814e10b62 -s sec-SoronfkrzKCk0CJkD1hTqrINtFGVNH705CXvetQNH4L1T98G
      Preparing REST benckmark ...
      Running 20 client benchmark test @ http://rest.yunba.io:8080/
         topic:yunba_rest_benchmark, appkey:5520a2887e353f5814e10b62
         avg: 191 ms, max: 244 ms, min: 168 ms, timeout: 0

输出结果有： 平均延时，最大延时，最小延时和超时客户端数量。

已经注册过的客户端数据会被重复使用，数据在 `scripts/rest_benchmark_data` 下。