#! /usr/bin/env escript
%%! -smp enable debug verbose -Wall -pz ebin/ deps/gen_logger/ebin deps/goldrush/ebin deps/lager/ebin deps/ibrowse/ebin deps/jiffy/ebin deps/snowflake/ebin deps/emqttc/ebin/ deps/getopt/ebin/ deps/lager/ebin deps/goldrush/ebin

-include("deps/emqttc/include/emqttc_packet.hrl").
-include("deps/emqttc/include/emqttc_yunba_misc.hrl").

-compile(export_all).

-define(REST_TEST_APPKEY, <<"5520a2887e353f5814e10b62">>).
-define(REST_TEST_SECKEY, <<"sec-SoronfkrzKCk0CJkD1hTqrINtFGVNH705CXvetQNH4L1T98G">>).
-define(REST_TEST_CLIENTID, <<"0000001528-000149024234">>).
-define(REST_TEST_USERNAME, <<"2733352468906682368">>).
-define(REST_TEST_PASSWORD, <<"3104d441f51d8">>).
-define(REST_TEST_TOPIC, <<"rest_test">>).

main(Args) ->
    case length(Args) =:= 2 of
        true ->

            [Front, RestAddr] = Args,
            pre_start(),
            do_rest_publish(Front, RestAddr);
        false ->
            io:format("./rest_test.erl <front> <rest_addr>~n")
    end.

pre_start() ->
    {ok, _} = application:ensure_all_started(ibrowse),
    {ok, _} = application:ensure_all_started(snowflake).

do_rest_publish(Front, RestAddr) ->
    {ok, Client} = emqttc:start_link([{host, Front}, {port, 1883}, {client_id, ?REST_TEST_CLIENTID},
                                      {proto_ver, ?MQTT_PROTO_V31_YUNBA}, {username, ?REST_TEST_USERNAME},
                                      {password, ?REST_TEST_PASSWORD}, {logger, debug}]),
    %% subscribe the test topic
    emqttc:subscribe(Client, ?REST_TEST_TOPIC, ?QOS_0),
    receive
        {suback, PacketId} ->
            io:format("Received suback of ~p~n", [PacketId])
    after
        ?TEST_TIMEOUT ->
            io:format("Waiting for suback timeout")
    end,

    RestURL = "http://" ++ RestAddr,
    PostContent = jiffy:encode({[
        {<<"method">>, <<"publish">>},
        {<<"appkey">>, ?REST_TEST_APPKEY},
        {<<"seckey">>, ?REST_TEST_SECKEY},
        {<<"topic">>, ?REST_TEST_TOPIC},
        {<<"msg">>, <<"HELLO42">>},
        {<<"opts">>, {[{<<"time_to_live">>, 0}]}}
    ]}),
    case emqttc_utils:http_post(RestURL, PostContent) of
        {ok, Body} ->
            BodyBin = emqttc_utils:to_bin(Body),
            {Response} = jiffy:decode(BodyBin),
            Status = proplists:get_value(<<"status">>, Response),
            case Status of
                0 ->
                    ok;
                _ ->
                    {error, Response}
            end;
        {error, Reason} ->
            {error, Reason}
    end,

    %% waiting for receiving the message that we published
    receive
        {publish, Topic, Payload} ->
            io:format("Message Received from ~s: ~p~n", [Topic, Payload])
    after
        ?TEST_TIMEOUT ->
            io:format("Error: receive timeout!~n")
    end,

    %% unsubscribe the topic
    emqttc:unsubscribe(Client, ?REST_TEST_TOPIC),

    %% disconnect the broker after finishing the simple test
    emqttc:disconnect(Client).
